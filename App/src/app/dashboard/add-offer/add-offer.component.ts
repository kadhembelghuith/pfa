import { Component, OnInit } from '@angular/core';
import { OffreService } from 'src/app/shared/offre.service';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.component.html',
  styleUrls: ['./add-offer.component.scss']
})
export class AddOfferComponent implements OnInit {
  showSuccessMessage: Boolean
  serverErrorMessages: String;
  skills1 = [
    { name: "c" },
    { name: "html" },
    { name: "css" },
    { name: "Python" }, { name: "java" }, { name: "Angular" }, { name: "PHP" }, { name: "C++" }, { name: "Laravel" }, { name: "NodeJS" }, { name: "ReactJs" }
  ]
  userDetails1;

  constructor(public offreService: OffreService,public userService:UserService) { }
  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(

      res => {
        this.userDetails1 = res["user"];
      },
      err => {
      }
  
    );
  }
  onSubmit(form: NgForm) {
    this.offreService.postOffre(form.value).subscribe(
      res => {
        this.showSuccessMessage = true;
        setTimeout(() => this.showSuccessMessage = false, 4000);
        form.resetForm();


      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>')
        }
        else {
          this.serverErrorMessages = err
        }

      }
    )

  }
}
