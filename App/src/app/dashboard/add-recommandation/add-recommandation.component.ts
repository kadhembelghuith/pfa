import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { NgForm } from '@angular/forms';
import { Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-add-recommandation',
  templateUrl: './add-recommandation.component.html',
  styleUrls: ['./add-recommandation.component.scss']
})
export class AddRecommandationComponent implements OnInit {

  showSuccessMessage: boolean;
  serverErrorMessages: string
  constructor(public userService: UserService,public router:Router,private route: ActivatedRoute) {
  }
  userDetails;
  userDetails1;

  ngOnInit(): void {
    this.route.params.pipe(switchMap((params: Params) => {
      return  this.userService.getUser(params['_id']); }))
   .subscribe(user => { this.userDetails = user['user'];  })
   this.userService.getUserProfile().subscribe(

    res => {
      this.userDetails1 = res["user"];
    },
    err => {
    }

  );
  }
  
  onSubmit(form: NgForm) {
    this.userService.addRecommandation(form.value,this.userDetails._id).subscribe(
      res => {
        this.showSuccessMessage = true;
        setTimeout(() => this.showSuccessMessage = false, 4000);
        form.resetForm();


      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>')
        }
        else {
          this.serverErrorMessages = err
        }

      }
    )

  }

}
