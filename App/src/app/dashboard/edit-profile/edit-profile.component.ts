import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { NgForm } from '@angular/forms';
import { Router, Params } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
const URL = 'http://localhost:3000/api/uploadImage';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({
    url: URL,
    itemAlias: 'image'
  });
  skills1 = [
    { name: "c" },
    { name: "html" },
    { name: "css" },
    { name: "Python" }, { name: "java" }, { name: "Angular" }, { name: "PHP" }, { name: "C++" }, { name: "Laravel" }, { name: "NodeJS" }, { name: "ReactJs" }
  ]
  constructor(public userService: UserService, public router: Router, private toastr: ToastrService) { }
  showSuccessMessage: boolean;
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  userDetails;
  serverErrorMessages: String;
  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(

      res => {
        this.userDetails = res["user"];
      },
      err => {
      }

    );
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
      this.toastr.success('File successfully uploaded!');
    };
  }
  onSubmit(form: NgForm) {
    this.userService.updateUser(form.value, this.userDetails._id).subscribe(
      res => {

        form.resetForm();
        this.router.navigate(['../userprofile']);


      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>')
        }
        else {
          this.serverErrorMessages = err
        }

      }
    )

  }
  isStudent() {
    return this.userDetails.role == "Student"
  }
  isProf() {
    return this.userDetails.role == "Enseignant"
  }  
  isIndustrial() {
    return this.userDetails.role == "industriel"
  }
  getImageName() {
    return this.userDetails.username + '.' + this.userDetails.image.split('.')[1];
  }
}
