import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service'
import { Router } from '@angular/router'
import { DashboardComponent } from '../dashboard.component'
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  userDetails;
  constructor(public userService: UserService, public router: Router) { }

  ngOnInit(): void {


  }
  isStudent() {
    return this.userDetails.role == "Student"
  }
  isProf() {
    return this.userDetails.role == "Enseignant"
  }
  isIndus() {
    return this.userDetails.role == "Industriel"
  }

}
