import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { User } from 'src/app/shared/user.model';

@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.scss']
})
export class SelectUserComponent implements OnInit {
  users: User[];

  searchText;
  constructor(private userService: UserService) {


  }
  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users)
  }

}
