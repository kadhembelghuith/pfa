import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service'
import { Router } from '@angular/router'
import { from } from 'rxjs';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
const URL = 'http://localhost:3000/api/uploadImage';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  userDetails;
  public uploader: FileUploader = new FileUploader({
    url: URL,
    itemAlias: 'image'
  });
  constructor(public userService: UserService, public router: Router, private toastr: ToastrService) { }
  userImageName
  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(

      res => {
        this.userDetails = res["user"];
      },
      err => {
      }

    );
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
      this.toastr.success('File successfully uploaded!');
    };
  }
  getImageName() {
    return this.userDetails.username + '.' + this.userDetails.image.split('.')[1];
  }
  isStudent() {
    return this.userDetails.role == "Student" || this.userDetails.role == "student"
  }
  isIndustrial() {
    return this.userDetails.role == "industriel"
  }
  onLogout() {
    this.userService.deleteToken();
    this.router.navigate(['/login']);
  }
}
