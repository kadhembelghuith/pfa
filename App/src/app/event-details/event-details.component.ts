import { Component, OnInit } from '@angular/core';
import { Event } from '../shared/event.model';
import { EventService } from '../shared/event.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {
  event: Event;
  eventDetails
  constructor(public eventService: EventService, private sanitizer: DomSanitizer, public router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(switchMap((params: Params) => {
      return this.eventService.getEvent(params['_id']);
    }))
      .subscribe(event => { this.eventDetails = event['event']; })
  }
  getUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.eventDetails.form + "?embedded=true")
  }
  getSocialMedia() {
    if (this.eventDetails.facebook || this.eventDetails.site_web)
      return true
  }
  getFacebook() {
    if (this.eventDetails.facebook)
      return true
  }
  getSite() {
    if (this.eventDetails.site_web)
      return true
  }
}
