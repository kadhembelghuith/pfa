export class Event {
    nom: String;
    club: String;
    started_date: Date;
    end_date: Date;

    contact: String;
    description: String;
    form: String;
    cover: String;
    logo: String;
    site_web: String;
    facebook: String;
}