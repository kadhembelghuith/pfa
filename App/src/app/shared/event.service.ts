import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from './event.model'
@Injectable({
  providedIn: 'root'
})
export class EventService {
  selectedEvent: Event = {
    nom: "",
    club: '',
    started_date: new Date(),
    end_date: new Date(),
    contact: '',
    description: '',
    form: '',
    cover: '',
    logo: '',
    site_web: '',
    facebook: ''
  }
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(public http: HttpClient) { }
  postEvent(event: Event) {
    return this.http.post(environment.apiBasedUrl + '/addEvent', event, this.noAuthHeader);
  }
  getEvents() {
    return this.http.get<Event[]>(environment.apiBasedUrl + '/getevents')

  }
  getEvent(_id: string): Observable<Event> {
    return this.http.get<Event>(environment.apiBasedUrl + '/eventDetails/' + _id)
  }
}
