import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';
import { switchMap } from 'rxjs/operators';
import { Router, Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
user:User;userDetails;
  constructor(public userService:UserService,public router:Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(switchMap((params: Params) => {
       return  this.userService.getUser(params['_id']); }))
    .subscribe(user => { this.userDetails = user['user'];  })
    
  }

}
