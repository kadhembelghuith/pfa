const mongoose = require('mongoose');
const Postulation=require('./postulation.model')
var offreSchema = new mongoose.Schema({
    indus: { type: String },

    name: { type: String },
    skills: [{ type: String }],
    description: { type: String },
    postulations: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Postulation', index: true
    }],
    saltSecret: String
})
mongoose.model('Offre', offreSchema);
