const mongoose = require('mongoose');
var postulationSchema = new mongoose.Schema({
    user: { type: String },
    motivation: { type: String },
    cv: { type: String },
    
    saltSecret: String
})
mongoose.model('Postulation', postulationSchema);
