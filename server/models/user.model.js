const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const Recommandation = require('./recommandation.model')
const Offre = require('./offre.model')

var userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: 'Full name can\'t be empty'
    },
    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    password: {
        type: String,
        required: 'Password can\'t be empty',
        minlength: [4, 'Password must be atleast 4 character long']
    },
    cin: {
        type: String,

    },
    nom: {
        type: String,
    },
    prenom: {
        type: String,

    },
    nom_societe: {
        type: String,

    },
    sexe: {
        type: String,

    },
    adresse: {
        type: String,

    },
    num_tel: {
        type: String,

    },
    site_web: {
        type: String,

    },
    specialité: {
        type: String,

    },
    image: {
        type: String,

    },
    skills: [{
        type: String,

    }],
    recommandations: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Recommandation', index: true
    }],
    section: { type: String }
    , role: { type: String },
    offres: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Offre', index: true
    }],
    saltSecret: String
});

// Custom validation for email
userSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

// Events
userSchema.pre('save', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});


// Methods
userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign({ _id: this._id },
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXP
        });
}



mongoose.model('User', userSchema);