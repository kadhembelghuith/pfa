var express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = mongoose.model('User');



var router = express.Router();
router.use(bodyParser.json());

const Emploi = require('../models/emploi');


/* GET Emploi */
router.get('/add', function(req, res, next) {
    res.render('schedule');
});

router.get('/view', function(req, res, next) {
    Emploi.find({})
    .then((emploi) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(emploi);
    }, (err) => next(err))
    .catch((err) => next(err));
});

router.post('/add', (req, res, next)=>{
    let groupe = req.body.groupe;
    let matiere = req.body.matiere;
    let horaire = req.body.horaire;
    let prof = req.body.prof;
    let jour = req.body.jour;
    let salle = req.body.salle;
    let anneescolaire = "2019-2020";

    Emploi.findOne({'groupe': groupe,'jour': jour,'horaire': horaire}, (err, data)=>{
        if(!err && data){
            console.log('Chreno deja plein !!');
            res.redirect('/');
        } else if(err) {
            err = new Error('Chreno Plein !');
            err.status = 404;
            return next(err);
        } else {
            let schedule = new Emploi({groupe,matiere,anneescolaire,jour,horaire,prof,salle});
            console.log(schedule);
            Emploi.create(schedule)
            .then((emploi) => { 
                console.log('Emploi Created ', emploi);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.redirect('/');
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    });

});

router.put('/', (req, res, next)=>{
    res.statusCode = 403;
     res.end('PUT operation not supported on /emploi');
});

router.delete('/view', (req, res, next)=>{
    Emploi.remove({})
     .then((resp) => {
         res.statusCode = 200;
         res.setHeader('Content-Type', 'application/json');
         res.json(resp);
     }, (err) => next(err))
     .catch((err) => next(err));  
});




// * Notification * 

router.get('/notify', function(req, res, next) {

    var time = new Date();
    var hours = new Date().getHours();
    var minutes = new Date().getMinutes();
    var jour = new Date().getUTCDay();
    horaire = "8h15-10h";

    if ((hours == 8 && minutes >= 15) || (hours == 10 && minutes < 15) || (hours == 9)) {
        horaire = "10h15-12h"
    }
    if ((hours == 10 && minutes >= 15) || (hours < 14 && hours > 10)) {
        horaire = "14h-15h45"
    }
    if ((hours == 15 && minutes < 45) || (hours == 14)) {
        horaire = "16h-17h45"
    }
    if ((hours == 17 && minutes >= 45) || (hours >= 18)) {
        jour++;
        horaire = "8h15-10h";

    }

    switch (jour) {
        case 1:
            jour = 'Lundi';
            break;
        case 2:
            jour = 'Mardi';
            break;
        case 3:
            jour = 'Mercredi';
            break;
        case 4:
            jour = 'Jeudi';
            break;
        case 5:
            jour = 'Vendredi';
            break;
        case 6:
            jour = 'Samedi';
            break;
        case 7:
            jour = 'Dimanche';
            break;

    }
 
                Emploi.findOne({ groupe: 'GI2S4', jour: jour, horaire: horaire }, (err, data) => {
                    if (!err && data) {
                        if (data.jour === "Dimanche") {
                            let notification = "Dimanche Pas de Cours !!";
                            res.statusCode = 200;

                            res.send({ 'notification': notification })
                        }
                        else {
                            console.log('Chreno FOUND !!');
                            let notification = "You have a " + data.matiere + " Course at " + data.horaire + " in " + data.salle + " !!";
                            res.statusCode = 200;

                            res.send({ 'notification': notification })
                        }


                    } else if (err) {
                        err = new Error('Emploi vide !');
                        err.status = 404;
                        return next(err);
                    }
                    else {
                        console.log('Pas de chreno ')
                    }

                });
        
});
router.get('/show', function(req, res, next) {
    let groupe ='GI2S4';
    let semaine = ['Lundi', 'Mardi','Mercredi', 'Jeudi','Vendredi', 'Samedi'];
    let temps= ['8H15', '10H15','14H', '16H'];
    x=0;
    matieres=[];
    Emploi.find({'groupe':groupe},(err, data) => {
        console.log(data);
        res.render('show', {data: data}); 
    })
    // for(const jour in semaine){
    //     for (const horaire in temps){
    //         Emploi.findOne({'groupe': groupe,'jour': jour,'horaire': horaire}, (err, data)=>{
    //             if(!err && data){
    //                 x++;
    //                 res.render('show',{matiere:data.matiere, prof:data.prof, salle:data.salle,});
    //             } else {
                    
    //             }
    //         })
    //     }
    // }
    
    
    
});


module.exports = router;